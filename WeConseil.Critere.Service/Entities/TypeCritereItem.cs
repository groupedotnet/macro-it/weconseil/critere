using Macro.Common;

namespace WeConseil.Critere.Service.Entities
{

    public class TypeCritereItem : IEntity
    {
        public Guid Id { get; set; }
        public string Designation { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public Boolean IsDeleted { get; set; }
    }
}