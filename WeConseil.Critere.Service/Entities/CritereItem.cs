using Macro.Common;

namespace WeConseil.Critere.Service.Entities
{

    public class CritereItem : IEntity
    {
        public Guid Id { get; set; }
        public string Designation { get; set; }
        public string Value { get; set; }
        public DateTimeOffset CreateDate { get; set; }
        public Boolean IsDeleted { get; set; }
       /* public  Guid IdTypeCritere { get; set; }
         public Guid IdOffer	 { get; set; }*/

    }
}