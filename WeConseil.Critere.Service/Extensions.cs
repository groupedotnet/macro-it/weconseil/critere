using WeConseil.Critere.Service.Dtos;
using WeConseil.Critere.Service.Entities;

namespace WeConseil.Critere.Service
{
    public static class Extensions
    {
        public static CritereItemDtos AsDto (this CritereItem critereItemItem)
        {
             return new CritereItemDtos(critereItemItem.Id,critereItemItem.Designation, critereItemItem.Value);
        }
    }
}