using Microsoft.AspNetCore.Mvc;
using Macro.Common;
using WeConseil.Critere.Service.Dtos;
using WeConseil.Critere.Service.Entities;


namespace WeConseil.Critere.Service.Controllers
{
    //https://localhost:7002/swagger/index.html
    [ApiController]
    [Route("Criters")]
    public class CriteresController : ControllerBase
    {
        private readonly IRepository<CritereItem> critereRepository;
        public CriteresController(IRepository<CritereItem> critereRepository)
        {
            this.critereRepository = critereRepository;
        }
        //GET /typeCriters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CritereItemDtos>>> GetAsync()
        {
            var items = (await critereRepository.GetAllAsync()).Select(item => item.AsDto());
            return Ok(items);
        }
        //GET /typeCriters/123456
        [HttpGet("{id}")]
        public async Task<ActionResult<CritereItemDtos>> GetByIdAsync(Guid id)
        {
            var item = await critereRepository.GetAsync(id);
            if (item == null)
            {
                return NotFound();
            }
            return item.AsDto();
        }
        //POST /typeCriters
        [HttpPost]
        public async Task<ActionResult<CritereItemDtos>> PostAsync(CreateCritereItemDto createCritereItemDto)
        {
            var itemCritere = new CritereItem
            {
                Designation = createCritereItemDto.Designation,
                Value = createCritereItemDto.Value,
                CreateDate = DateTimeOffset.UtcNow,
                IsDeleted = false

            };
            await critereRepository.CreateAsync(itemCritere);
            return CreatedAtAction(nameof(GetByIdAsync), new { id = itemCritere.Id }, itemCritere);
        }
        //Update /typeCriters
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(Guid id, UpdateCritereItemDtos updateCritereItemDtos)
        {
            var existingCritereItem = await critereRepository.GetAsync(id);
            if (existingCritereItem == null)
            {
                return NotFound();
            }
            existingCritereItem.Designation = updateCritereItemDtos.Designation;
            existingCritereItem.Value = updateCritereItemDtos.Value; 
            await critereRepository.UpdateAsync(existingCritereItem);
            return NoContent();
        }
        //Update /typeCriters
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            var existingCritereItem = await critereRepository.GetAsync(id);
            if (existingCritereItem == null)
            {
                return NotFound();
            }
            existingCritereItem.IsDeleted = true;
            await critereRepository.UpdateAsync(existingCritereItem);
            return NoContent();
        }
    }
}