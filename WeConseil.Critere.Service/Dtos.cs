using System.ComponentModel.DataAnnotations;

namespace WeConseil.Critere.Service.Dtos
{
    public record CritereItemDtos(Guid Id, string Designation, string Value);
    public record CreateCritereItemDto([Required][MinLength(3)][MaxLength(60)] string Designation,[Required][MinLength(3)][MaxLength(60)] string Value);
    public record UpdateCritereItemDtos([Required][MinLength(3)][MaxLength(60)] string Designation ,[Required][MinLength(3)][MaxLength(60)] string Value);
}