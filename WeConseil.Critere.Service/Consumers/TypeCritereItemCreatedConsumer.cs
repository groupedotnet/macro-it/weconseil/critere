using MassTransit;
using Macro.Common;
using WeConseil.TypeCritere.Contracts;
using WeConseil.Critere.Service.Entities;

namespace WeConseil.Critere.Service.Consumers
{
    public class TypeCritereItemCreatedConsumer : IConsumer<TypeCritereItemCreated>
    {
        private readonly IRepository<TypeCritereItem> repository;
        public TypeCritereItemCreatedConsumer(IRepository<TypeCritereItem> repository)
        {
            this.repository = repository;
        }
        public async Task Consume(ConsumeContext<TypeCritereItemCreated> context)
        {
            var message = context.Message;
            var item = await repository.GetAsync(message.TypeCritereId);
           if (item != null)
            {
                return;
            }
                item = new TypeCritereItem{
                Id = message.TypeCritereId,
                Designation = message.Designation,
                IsDeleted = false
            };
            await repository.CreateAsync(item);

        }
    }
}