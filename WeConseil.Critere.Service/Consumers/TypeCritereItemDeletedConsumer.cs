using MassTransit;
using Macro.Common;
using WeConseil.TypeCritere.Contracts;
using WeConseil.Critere.Service.Entities;

namespace WeConseil.Critere.Service.Consumers
{
    public class TypeCritereItemDeletedConsumer : IConsumer<TypeCritereItemDeleted>
    {
        private readonly IRepository<TypeCritereItem> repository;
        public TypeCritereItemDeletedConsumer(IRepository<TypeCritereItem> repository)
        {
            this.repository = repository;
        }
        public async Task Consume(ConsumeContext<TypeCritereItemDeleted> context)
        {
            var message = context.Message;
            var item = await repository.GetAsync(message.TypeCritereId);
            if (item == null)
            {
               return;
            }
                await repository.RemoveAsync(message.TypeCritereId);

        }
    }
}