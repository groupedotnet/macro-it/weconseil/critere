using MassTransit;
using Macro.Common;
using WeConseil.Critere.Service.Entities;
using  WeConseil.TypeCritere.Contracts;

namespace WeConseil.Critere.Service.Consumers
{
    public class TypeCritereItemUpdatedConsumer : IConsumer<TypeCritereItemUpdated>
    {
        private readonly IRepository<TypeCritereItem> repository;
        public TypeCritereItemUpdatedConsumer(IRepository<TypeCritereItem> repository)
        {
            this.repository = repository;
        }
        public async Task Consume(ConsumeContext<TypeCritereItemUpdated> context)
        {
           var message = context.Message;
            var item = await repository.GetAsync(message.TypeCritereId);
            if (item == null)
            {
                 item = new TypeCritereItem{
                Id = message.TypeCritereId,
                Designation = message.Designation,
                IsDeleted = false
            };
            await repository.CreateAsync(item);
            }
            else
            {
                item.Id = message.TypeCritereId;
               item. Designation = message.Designation;
                await repository.UpdateAsync(item);
            }

        }
    }
}